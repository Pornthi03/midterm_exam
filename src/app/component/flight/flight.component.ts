import { Component, Injectable,OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { PageService } from 'src/app/share/page.service';
import { Flight } from './flight';
import {
  NgbDateStruct,
  NgbCalendar,
  NgbDatepickerI18n,
  NgbCalendarBuddhist,
  NgbDate,
  NgbDateParserFormatter
} from '@ng-bootstrap/ng-bootstrap';
import localeThai from '@angular/common/locales/th';
import { getLocaleDayNames, FormStyle, TranslationWidth, getLocaleMonthNames, formatDate, registerLocaleData } from '@angular/common';

@Injectable()
export class NgbDatepickerI18nBuddhist extends NgbDatepickerI18n {

  private _locale = 'th';
  private _weekdaysShort: readonly string[];
  private _monthsShort: readonly string[];
  private _monthsFull: readonly string[];

  constructor() {
    super();

    registerLocaleData(localeThai);

    const weekdaysStartingOnSunday = getLocaleDayNames(this._locale, FormStyle.Standalone, TranslationWidth.Short);
    this._weekdaysShort = weekdaysStartingOnSunday.map((day, index) => weekdaysStartingOnSunday[(index + 1) % 7]);

    this._monthsShort = getLocaleMonthNames(this._locale, FormStyle.Standalone, TranslationWidth.Abbreviated);
    this._monthsFull = getLocaleMonthNames(this._locale, FormStyle.Standalone, TranslationWidth.Wide);
  }

  getMonthShortName(month: number): string { return this._monthsShort[month - 1] || ''; }

  getMonthFullName(month: number): string { return this._monthsFull[month - 1] || ''; }

  getWeekdayLabel(weekday: number) {
    return this._weekdaysShort[weekday - 1] || '';
  }

  getDayAriaLabel(date: NgbDateStruct): string {
    const jsDate = new Date(date.year, date.month - 1, date.day);
    return formatDate(jsDate, 'fullDate', this._locale);
  }

  override getYearNumerals(year: number): string { return String(year); }
}


@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css'],
  providers: [
    { provide: NgbCalendar, useClass: NgbCalendarBuddhist },
    { provide: NgbDatepickerI18n, useClass: NgbDatepickerI18nBuddhist },
  ],
})
export class FlightComponent implements OnInit {

  flight : Flight;
  sflights !: Flight[];
  flightForm : FormGroup;
  hoveredDate: NgbDate | null = null;

  selectControl:FormControl = new FormControl();

  constructor(private fb: FormBuilder,private pageService: PageService,private calendar: NgbCalendar, public formatter: NgbDateParserFormatter) {
    this.flight = new Flight("","","","",calendar.getToday(),calendar.getNext(calendar.getToday(), 'd', 8),1,0,0);
    this.flightForm = this.fb.group({
        fullname:['',[Validators.required,Validators.pattern("^[A-Z]'?[-a-zA-Z\\s]+$")]],
        from: ['',Validators.required],
        to: ['',Validators.required],
        type: ['',Validators.required],
        departure: [calendar.getToday(),Validators.required],
        arrival: [calendar.getNext(calendar.getToday(), 'd', 8)],
        adults: ['',[Validators.required,Validators.pattern("[0-9]*$")]],
        children: ['',Validators.pattern("[0-9]*$")],
        infants: ['',Validators.pattern("[0-9]*$")],
    });
    this.getPage();
  }

  getPage(){
    this.sflights = this.pageService.getFlight();
  }

  onSubmit(f: Flight): void {
    this.pageService.addFlight(f);
  }


  airport = [
    {value: 'Yeosu Airport',viewValue: 'Yeosu Airport'},
    {value: 'คริสเตียนซานด์ Airport',viewValue: 'คริสเตียนซานด์ Airport'},
    {value: 'ซิดนีย์ Airport',viewValue: 'ซิดนีย์ Airport'},
    {value: 'เชียงราย Airport',viewValue: 'เชียงราย Airport'},
    {value: 'ตรัง Airport',viewValue: 'ตรัง Airport'},
    {value: 'หัวหิน Airport',viewValue: 'หัวหิน Airport'},
    {value: 'นางาซากิ Airport',viewValue: 'นางาซากิ Airport'},
    {value: 'เจนีวา Airport',viewValue: 'เจนีวา Airport'},
    {value: 'ปูซาน Airport',viewValue: 'ปูซาน Airport'}
  ];


  ngOnInit(): void {
  }

  onDateSelection(date: NgbDate) {
    if (!this.flightForm.value.departure && !this.flightForm.value.arrival) {
      this.flightForm.value.departure = date;
    } else if (this.flightForm.value.departure && !this.flightForm.value.arrival && date && date.after(this.flightForm.value.departure)) {
      this.flightForm.value.arrival = date;
    } else {
      this.flightForm.value.arrival = null;
      this.flightForm.value.departure = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.flightForm.value.departure && !this.flightForm.value.arrival && this.hoveredDate && date.after(this.flightForm.value.departure) &&
        date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) { return this.flightForm.value.arrival && date.after(this.flightForm.value.departure) && date.before(this.flightForm.value.arrival); }

  isRange(date: NgbDate) {
    return date.equals(this.flightForm.value.departure) || (this.flightForm.value.arrival && date.equals(this.flightForm.value.arrival)) || this.isInside(date) ||
        this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

}
