import { NgbDate } from "@ng-bootstrap/ng-bootstrap";

export class Flight {
  constructor(
  public fullName: string,
  public from: string,
  public to: string,
  public type: string,
  public departure: NgbDate | null,
  public arrival: NgbDate | null,
  public adults: number,
  public children: number,
  public infants: number,
  ) {}
  }
