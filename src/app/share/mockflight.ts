import { Flight } from '../component/flight/flight';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';

export class Mockflight {
  public static mFlights: Flight[] = [{
    fullName: "Pornthida Phobai",
    from: "เจนีวา Airport",
    to: "ปูซาน Airport",
    type: "Return",
    departure: NgbDate.from({year: 2565, month: 3, day: 8}),
    arrival: NgbDate.from({year: 2655, month: 3, day: 14}),
    adults: 1,
    children: 0,
    infants: 0,
  }
  ]
}
