import { Injectable } from '@angular/core';
import { Flight } from '../component/flight/flight';
import { Mockflight } from '../share/mockflight';
@Injectable({
  providedIn: 'root'
})
export class PageService {
  flights: Flight[] = [];
  constructor() {
    this.flights = Mockflight.mFlights;
  }

  getFlight():Flight[]{
    console.log(this.flights);
    return this.flights;
  }

  addFlight(f:any):void{
    this.flights.push({
      fullName: f.fullname,
      from: f.from,
      to: f.to,
      type: f.type,
      departure: f.departure,
      arrival: f.arrival,
      adults: f.adults,
      children: f.children,
      infants: f.infants
    });

  }

}
